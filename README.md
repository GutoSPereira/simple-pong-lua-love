# Pong simplificado
O presente experimento é um objeto de estudos para desenvolvido para um programa de Iniciação Científica, no Laboratório de Interfaces Físicas Esperimentais (LIFE) situado na Pontifícia Universidade Católica do Rio de Janeiro (PUC-Rio).

Durante uma das aulas da disciplina Interfaces Físicas e Lógicas, ministrada pelos professores João Bonelli e Luiz Ludwig, é desenvolvido em conjunto com os alunos um código que emula um dos primeiros jogos eletrônicos da história: **Pong**. O jogo é desenvolvido no ambiente [Processing](http://processing.org/) durante uma dinâmica na qual os alunos completam o código de acordo com a função, condição, reação ou ação desejada.

A proposta deste experimento é transpor o código escrito durante esta atividade para a linguagem Lua da forma mais fiel possível, além de apontar, por meio de comentários, sempre que aplicável, as interseções entre as duas sintaxes e métodos.

Adicionalmente, um exemplo semelhante foi desenvolvido; porém, esta versão tem como objetivo fazer uma pequena demonstração das capacidades audiovisuais da game engine Löve 2D em conjunto com Lua, mesmo em uma aplicação de complexidade tão básica tanto a nível lógico quanto de interação. Este repositório pode ser acessado [clicando aqui](https://bitbucket.org/GutoSPereira/pong-lua-love).

Em ambos os exemplos, o jogador utiliza seu ponteiro do mouse para movimentar uma "raquete" (direita). Ao deixar a bola ultrapassar a raquete, o jogador é comunicado de que perdeu o jogo.

Já na versão mais complexa, ao atingir 5 pontos (acertos), o usuário é congratulado com uma mensagem, e em ambas as situações apresenta a chance de tentar novamente (ao teclar "enter"/"return") ou sair (ao teclar "escape").
function love.load() -- análogo ao void setup() no processing, executado exatamente 1 vez, no início do programa
  bolaX = 300
  bolaY = 200
  veloX = -1
  veloY = 1
  fase = 0

  love.window.setMode(600, 400)
  --[[
    análogo ao size(600, 400) no processing, podendo ser adicionado ao final
    o parâmetro flags -> {fullscreen=true,borderless=true ... }
  ]]
  
  width, height, flags = love.window.getMode() -- returna os parâmetros da tela para as variáveis em sequencia, largura, altura e flags respectivamente
  myFont = love.graphics.newFont(25)-- análogo a loadFont() no processing, porém carrega a fonte default quando não passado o nome do arquivo como parâmetro
  love.graphics.setFont(myFont)-- análogo ao textFont() no processing
end

function love.update()-- loop de atualização das variáveis
  mouseX, mouseY = love.mouse.getPosition()-- a cada iteração do loop guarda as posições x e y do mouse, respectivamente às variáveis listadas

  bolaX = bolaX + veloX-- não há bolaX += veloX ou bolaX++, a variável deve ser atualizada desta forma
  bolaY = bolaY + veloY

  -- colisão embaixo
  if bolaY >= 400 - 20 then
    veloY = -veloY
  end

  -- colisão em cima
  if bolaY <= 0 then
    veloY = -veloY
  end

  -- colisão na raquete do pc
  if bolaX <= 40 then
    veloX = -veloX
  end

  -- colisão com a raquete do jogador
  if bolaX + 20 >= 560 then
    if bolaY + 20 >= mouseY - 40 and bolaY <= mouseY + 40 then
      veloX = -veloX
    else
      -- game over
      fase = 1
    end
  end
end

--[[
  As funções love.update() e lode.draw() trabalham em conjunto 
  para formar um análogo da função void draw() no processing. 
  Onde o love.update() é focado em alterações das variáveis e 
  o love.draw() para exibição dos objetos.
]]
function love.draw()-- loop de output visual
  if fase == 0 then
    -- jogo
    love.graphics.rectangle("fill", bolaX, bolaY, 20, 20) -- bola
    love.graphics.rectangle("fill", 580 - 20, mouseY - 40, 20, 80) -- raquete jogador
    love.graphics.rectangle("fill", 20, bolaY - 40 + 10, 20, 80) -- raquete computador
  end

  if fase == 1 then
    -- game over
    love.graphics.setBackgroundColor(255, 255, 255)-- análogo a background(255)
    love.graphics.setColor(0, 0, 0, 1)--análogo a fill(0)
    local loseText = "VOCÊ PERDEU!"
    love.graphics.printf(loseText, (width / 2) - (myFont:getWidth(loseText) / 2), (height / 2) - (myFont:getHeight(loseText) / 2),1000,"left")
    --[[
      esta função adiciona um texto na tela. esta função aparenta ser mais 
      complicada do que o necessário, principalmente para uma funcionalidade 
      tão simples, porém, é possível fazer com que este comportamento seja 
      atingido definindo outro tipo de função que obtenha o mesmo efeito de 
      uma forma mais simples. Parecido com o processing, talvez, com o uso 
      de text(), textAlign() e etc.
    ]]
  end
end